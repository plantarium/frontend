export default [
  {
    id: 'enableSync',
    type: 'boolean',
  },
  {
    id: 'enableRandomSeed',
    type: 'boolean',
  },
];
