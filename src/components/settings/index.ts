import './index.scss';
import { EventEmitter } from '@plantarium/helpers';
import template from './template';
import SettingsUI from 'settings-ui';

class SettingsView extends EventEmitter {
  private store = {};

  private ui = SettingsUI();
  private visible = false;
  private wrapper = document.getElementById('settings-wrapper');

  constructor() {
    super();

    this.ui.bind(template, this.store);

    this.ui.addChangeListener((key: string, value: string) => {
      this.emit(key, value);
      this.save();
    });

    this.ui.render().to(this.wrapper);

    this.load();

    document
      .getElementById('button-settings')
      .addEventListener('click', (ev) => {
        this.visible ? this.hide() : this.show();
      });
  }

  set(type: string, value: any) {
    this.store[type] = value;
    this.emit(type, value);
    this.save();
  }

  private load() {
    const settingsString = localStorage.getItem('settings');
    if (settingsString && settingsString !== '') {
      const settings = JSON.parse(settingsString);

      Object.entries(settings).forEach(([key, value]) => {
        this.store[key] = value;
      });

      this.ui.update();
    }
  }

  private save() {
    localStorage.setItem('settings', JSON.stringify(this.store));
  }

  private show() {
    this.visible = true;
    this.wrapper.classList.add('settings-visible');
  }

  private hide() {
    this.visible = false;
    this.wrapper.classList.remove('settings-visible');
  }
}

export default new SettingsView();
