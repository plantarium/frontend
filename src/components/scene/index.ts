import { ProjectManager } from 'components';
import { PlantariumSettings, PlantDescription } from '@plantarium/types';
import Renderer from '@plantarium/renderer';
import { Transform, Box, Program, Mesh, Color } from 'ogl';

import * as generator from '@plantarium/generator';

export default class Scene {
  renderer: Renderer;
  scene: Transform;

  program: Program;
  mesh: Mesh;

  constructor(pm: ProjectManager) {
    const canvas = document.getElementById(
      'render-canvas',
    ) as HTMLCanvasElement;

    this.renderer = new Renderer(canvas);
    this.renderer.handleResize();

    this.scene = this.renderer.scene;
    const { gl } = this.renderer;

    const geometry = new Box(gl);

    this.program = new Program(gl, {
      vertex: `
            attribute vec3 position;

            uniform mat4 modelViewMatrix;
            uniform mat4 projectionMatrix;

            void main() {
                gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
            }
            `,
      fragment: `

            precision highp float;

            uniform vec3 uColor;

            void main() {
                gl_FragColor.rgb = uColor;
                gl_FragColor.a = 1.0;
            }
        `,
      uniforms: {
        uColor: {
          value: new Color(1, 0, 0),
        },
      },
    });

    this.mesh = new Mesh(gl, { geometry, program: this.program });
    this.mesh.setParent(this.scene);

    pm.on('settings', this.setSettings.bind(this));

    pm.on('plant', this.setPlant.bind(this));
    this.setPlant(pm.getPlant());
  }

  setPlant(plant: any) {
    this.program.uniforms.uColor.value = new Color(plant.color);
    this.mesh.geometry = generator.cube(this.renderer.gl, plant.dims);
  }

  setSettings(settings: PlantariumSettings) {}
}
