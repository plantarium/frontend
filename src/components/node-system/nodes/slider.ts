export default {
  name: 'Slider',
  outputs: ['number'],
  state: {
    id: 'value',
    type: 'number',
    inputType: 'range',
    value: 0,
  },
  compute(_inputData: any[], { value = 0 }) {
    return value;
  },
};
