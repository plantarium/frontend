const dev = {
  meta: {
    lastSaved: Date.now(),
  },
  nodes: [
    {
      attributes: {
        pos: {
          x: 0,
          y: 0,
        },
        type: 'output',
        id: '0',
      },
    },
    {
      attributes: {
        pos: {
          x: -30,
          y: 0,
        },
        type: 'vec3',
        id: '1',
        refs: [
          {
            id: '0',
            out: 0,
            in: 0,
          },
        ],
      },
      state: { x: 1, y: 0, z: 1 },
    },
  ],
};

export default dev;
