export default {
  ground_enable: false,
  debug_pd: true,
  debug_skeleton: true,
  debug_grid: true,
  debug_grid_resolution: 7,
  debug_grid_size: 2,
  stemResY: 15,
};
